/*
 * RPG
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 * Copyright (C) 2022  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "GraphicGameObject.hpp"
#include "TextureManager.hpp"
#include "Game.hpp"
#include "Log.hpp"
#include "Settings.hpp"

RPG::GraphicGameObject::GraphicGameObject()
    :GameObject(), _Position(0,0),_Velocity(0,0), _Acceleration(0,0) {
}



void RPG::GraphicGameObject::Load(const std::string script){
}


void RPG::GraphicGameObject::Draw()
{
    //Es Wird nur angezeigt, wenn die lebensenergie größer 0 ist
    if (_Visable)
        return;
    TextureManager::Instance()->DrawFrame(_TextureID, (int)_Position.GetX(), (int)_Position.GetY(), _Width, _Height, _CurrentFrame, _CurrentRow);
    if (Settings::Instance()->GetCollisionBoxes()) {
        DrawRectangleLines(_ObjectCollision.x, _ObjectCollision.y, _ObjectCollision.width, _ObjectCollision.height, RED );
        DrawRectangleLines(_NorthCollision.x, _NorthCollision.y, _NorthCollision.width, _NorthCollision.height, BLUE );
        DrawRectangleLines(_EastCollision.x, _EastCollision.y, _EastCollision.width, _EastCollision.height, BLUE );
        DrawRectangleLines(_SouthCollision.x, _SouthCollision.y, _SouthCollision.width, _SouthCollision.height, BLUE );
        DrawRectangleLines(_WestCollision.x, _WestCollision.y, _WestCollision.width, _WestCollision.height, BLUE );
    }
}

void RPG::GraphicGameObject::Clean()
{
}

void RPG::GraphicGameObject::Update()
{
    if (_Visable)
        return;
}

Vector2D  RPG::GraphicGameObject::GetPosition()
{
    return _Position;
}

int RPG::GraphicGameObject::GetWidth()
{
    return _Width;
}


int RPG::GraphicGameObject::GetHeight()
{
    return _Height;
}

void RPG::GraphicGameObject::SetName(std::string name) {
    _Name = name;
}

std::string RPG::GraphicGameObject::GetName() {
    return _Name;
}

void RPG::GraphicGameObject::SetActivCollision(int activ) {
    if (activ >= 0 && activ <=4) {
        _ActivCollision = activ;
    }
}

int RPG::GraphicGameObject::GetActivCollision() {
    return _ActivCollision;
}

void RPG::GraphicGameObject::SetObject(Rectangle object) {
    _ObjectCollision = object;
}

Rectangle RPG::GraphicGameObject::GetObject() {
    return _ObjectCollision;
}

void RPG::GraphicGameObject::SetNorth(Rectangle north) {
    _NorthCollision = north;
}

Rectangle RPG::GraphicGameObject::GetNorth() {
    return _NorthCollision;
}

void RPG::GraphicGameObject::SetEast(Rectangle east) {
    _EastCollision = east;
}

Rectangle RPG::GraphicGameObject::GetEast() {
    return _EastCollision;
}

void RPG::GraphicGameObject::SetSouth(Rectangle south) {
    _SouthCollision = south;
}

Rectangle RPG::GraphicGameObject::GetSouth() {
    return _SouthCollision;
}

void RPG::GraphicGameObject::SetWest(Rectangle west) {
    _WestCollision = west;
}

Rectangle RPG::GraphicGameObject::GetWest() {
    return _WestCollision;
}

bool RPG::GraphicGameObject::CollisionDetect([[maybe_unused]] GraphicGameObject* obj) {
    return false;
}

void RPG::GraphicGameObject::SetPosition(float x, float y){
    _Position.SetX(x);
    _Position.SetY(y);
}

void RPG::GraphicGameObject::SetHeight(float h){
    _Height = h;
}

void RPG::GraphicGameObject::SetWidth(float w){
    _Width = w;
}

