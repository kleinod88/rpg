#ifndef __LEVELPARSER
#define __LEVELPARSER

#include <tinyxml2.h>
#include "RPGMap.hpp"
#include <string>

namespace RPG {

class RPGMapParser {
public:
    RPGMap* ParseRPGMap(std::string levelFile);

private:

    void ParseTilesets(tinyxml2::XMLElement* tilesetRoot, std::vector<Tileset>* tilesets);

    void ParseTileLayer(tinyxml2::XMLElement* tileElement, RPGMap *rmap);

    void ParseObjectLayer(tinyxml2::XMLElement* pObjectElement, RPGMap* level);


    int _TileSize;
    int _Width;
    int _Height;
};

}; //namespace RPG
#endif // __LEVELPARSER
