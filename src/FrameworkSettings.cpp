/*
 * RPG
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 * Copyright (C) 2022  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "FrameworkSettings.hpp"
#include "Log.hpp"
#define SOL_ALL_SAFETIES_ON 1
#include <sol/sol.hpp>
#include <config.h>


RPG::FrameworkSettings* RPG::FrameworkSettings::_Instance = nullptr;

RPG::FrameworkSettings* RPG::FrameworkSettings::Instance() {
    if (_Instance == nullptr){
        _Instance = new FrameworkSettings();
    }
    return _Instance;
}

RPG::FrameworkSettings::FrameworkSettings()
    : _ShowSplash(true),_StartToPlay(false),_ShowVersion(false)
{
}

bool RPG::FrameworkSettings::Load() {
    sol::state lua;
    lua.open_libraries(sol::lib::base);
    lua.open_libraries(sol::lib::string);

    lua.script_file (std::string (DATAPATH) + std::string ("Settings/") + std::string("FrameworkSettings.lua"));
    _ShowSplash = lua.get<bool>("ShowSplash");
    _StartToPlay = lua.get<bool>("StartToPlay");
    _ShowVersion = lua.get<bool>("ShowVersion");
    _PlayerAnimationSpeed = lua.get<int>("PlayerAnimationSpeed");

    return true;
}


bool RPG::FrameworkSettings::GetShowSplash(){
    return _ShowSplash;
}

bool RPG::FrameworkSettings::GetStartToPlay(){
    return _StartToPlay;
}

bool RPG::FrameworkSettings::GetShowVersion(){
    return _ShowVersion;
}

int RPG::FrameworkSettings::GetPlayerAnimationSpeed(){
    return _PlayerAnimationSpeed;
}
