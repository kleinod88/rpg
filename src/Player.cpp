#include "Player.hpp"
#include "TileLayer.hpp"
#include "Game.hpp"
#include "GameStateMaschine.hpp"
#include "TextureManager.hpp"
#include "FrameworkSettings.hpp"
#include <raylib.h>
#include <config.h>
#include "Log.hpp"

RPG::Player* RPG::Player::_Instance = nullptr;

RPG::Player * RPG::Player::Instance()
{
    if ( _Instance == nullptr ) {
        _Instance = new Player();
    }
    return _Instance;
}


RPG::Player::Player() :
    GraphicGameObject()
{
    _Position.SetX ( 2800 );
    _Position.SetY ( 800 );
    _Frame = 0;
    _Hearts = 5;
    _IsAttack = false;
}


void RPG::Player::Draw()
{
    int speed = GetFPS() / FrameworkSettings::Instance()->GetPlayerAnimationSpeed();
    if ( _AnimationToPlay != PLAYER_ANIMATION_TO_PLAY::NONE ) {
        if ( _Frame < speed *1 )
            _CurrentFrame = 0;
        else if ( _Frame < speed *2 )
            _CurrentFrame = 1;
        else if ( _Frame < speed * 3 )
            _CurrentFrame = 2;
        else if ( _Frame < speed * 4 )
            _CurrentFrame = 3;
        else {
            _AnimationToPlay = PLAYER_ANIMATION_TO_PLAY::NONE;
            _IsAttack = false;
            _Frame = 0;
        }
    } else {
        if ( _CurrentRow == 4 ) {
            _CurrentRow = 0;
        } else if ( _CurrentRow == 5 ) {
            _CurrentRow = 1;
        } else if ( _CurrentRow == 6 ) {
            _CurrentRow = 2;
        } else if ( _CurrentRow == 7 ) {
            _CurrentRow = 3;
        }
    }


    GraphicGameObject::Draw();
}

void RPG::Player::Update()
{
    if ( _Hearts <= 0 ) {
        GameStateMaschine::Instance()->ChangeState ( "Menu", "GameOver.menu" );
    }
    _Velocity.SetX ( 0 );
    _Velocity.SetY ( 0 );


    HandleInput();

    if ( _Frame < GetFPS() )
        _Frame++;
    else
        _Frame = 0;

    _Position += _Velocity;

    _ObjectCollision.x = _Position.GetX() + ( ( _Width - 10 ) /2 );
    _ObjectCollision.y = _Position.GetY() + ( _Height-10 );
    _NorthCollision.x = _Position.GetX()-2;
    _NorthCollision.y = _Position.GetY()-1;
    _EastCollision.x = _Position.GetX() + _Width - 2;
    _EastCollision.y = _Position.GetY() - 1;
    _SouthCollision.x = _Position.GetX() - 2;
    _SouthCollision.y = _Position.GetY() + _Height - 2;
    _WestCollision.x = _Position.GetX() - 2;
    _WestCollision.y = _Position.GetY() - 1;


}

void RPG::Player::Clean()
{
    //  DLOG << "clean player\n";
}

void RPG::Player::HandleInput()
{
        if ( IsGamepadButtonDown ( 0, GAMEPAD_BUTTON_LEFT_FACE_RIGHT ) ||IsKeyDown ( KEY_D ) ) {
            _Velocity.SetX ( 0.9f );
            if ( _AnimationToPlay == PLAYER_ANIMATION_TO_PLAY::NONE ) {
                _AnimationToPlay = PLAYER_ANIMATION_TO_PLAY::WALK;
                _Frame = 0;
                _Direction = 2;
                _CurrentRow = WALK_RIGHT;
            }

        }
        if ( IsGamepadButtonDown ( 0, GAMEPAD_BUTTON_LEFT_FACE_LEFT ) ||IsKeyDown ( KEY_A ) ) {
            _Velocity.SetX ( -0.9f );
            if ( _AnimationToPlay == PLAYER_ANIMATION_TO_PLAY::NONE ) {
                _AnimationToPlay = PLAYER_ANIMATION_TO_PLAY::WALK;
                _Frame = 0;
                _Direction = 4;
                _CurrentRow = WALK_LEFT;
            }
        }
        if ( IsGamepadButtonDown ( 0, GAMEPAD_BUTTON_LEFT_FACE_DOWN ) ||IsKeyDown ( KEY_S ) ) {
            _Velocity.SetY ( 0.9f );
            if ( _AnimationToPlay == PLAYER_ANIMATION_TO_PLAY::NONE ) {
                _AnimationToPlay = PLAYER_ANIMATION_TO_PLAY::WALK;
                _Frame = 0;
                _Direction = 3;
                _CurrentRow = WALK_DOWN;
            }
        }
        if ( IsGamepadButtonDown ( 0, GAMEPAD_BUTTON_LEFT_FACE_UP ) ||IsKeyDown ( KEY_W ) ) {
            _Velocity.SetY ( -0.9f );
            if ( _AnimationToPlay == PLAYER_ANIMATION_TO_PLAY::NONE ) {
                _AnimationToPlay = PLAYER_ANIMATION_TO_PLAY::WALK;
                _Frame = 0;
                _Direction = 1;
                _CurrentRow = WALK_UP;
            }
        }

    if ( IsGamepadButtonDown ( 0, GAMEPAD_BUTTON_RIGHT_FACE_RIGHT ) ||IsKeyDown ( KEY_RIGHT_SHIFT ) ) {
        _Velocity *= 4.0f;
    }
    if ( !IsGamepadButtonDown ( 0, GAMEPAD_BUTTON_RIGHT_FACE_RIGHT ) ||!IsKeyDown ( KEY_RIGHT_SHIFT ) ) {
        _Acceleration.SetX ( 0 );
    }


    if ( IsGamepadButtonPressed ( 0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN ) ||IsKeyPressed ( KEY_DOWN ) ) {
        _AnimationToPlay = PLAYER_ANIMATION_TO_PLAY::SWORT;
        PlaySound ( _SwordSound );
        _Frame = 0;
        _IsAttack = true;
        if ( _CurrentRow == WALK_UP )
            _CurrentRow = SWORT_UP;
        if ( _CurrentRow == WALK_DOWN )
            _CurrentRow = SWORT_DOWN;
        if ( _CurrentRow == WALK_LEFT )
            _CurrentRow = SWORT_LEFT;
        if ( _CurrentRow == WALK_RIGHT )
            _CurrentRow = SWORT_RIGHT;
    }
}

void RPG::Player::Load ( std::string scriptFile )
{
    GraphicGameObject::Load ( scriptFile );
    _Position.SetX(500);
    _Position.SetY(500);
    _Width = 18;
    _Height = 26;
    _TextureID = "player";
    _NumFrames = 5;

    _ObjectCollision.x = _Position.GetX() + ( ( _Width - 10 ) /2 );
    _ObjectCollision.y = _Position.GetY() + ( _Height-10 );
    _ObjectCollision.width = 10;
    _ObjectCollision.height = 10;

    _NorthCollision.x = _Position.GetX()-2;
    _NorthCollision.y = _Position.GetY()-1;
    _NorthCollision.width = _Width+4;
    _NorthCollision.height = 5;

    _EastCollision.x = _Position.GetX() + _Width - 2;
    _EastCollision.y = _Position.GetY() - 1;
    _EastCollision.width = 5;
    _EastCollision.height = _Height + 4;

    _SouthCollision.x = _Position.GetX() - 2;
    _SouthCollision.y = _Position.GetY() + _Height - 2;
    _SouthCollision.width = _Width + 4;
    _SouthCollision.height = 5;

    _WestCollision.x = _Position.GetX() - 2;
    _WestCollision.y = _Position.GetY() - 1;
    _WestCollision.width = 4;
    _WestCollision.height = _Height + 2;

    _IsAttack = false;

    TextureManager::Instance()->Load ( std::string(std::string(DATAPATH)+std::string("Heart.png")), "heart" );
    _SwordSound =  LoadSound (std::string(std::string(DATAPATH)+std::string("Sword.wav")).c_str());
}

bool RPG::Player::CollisionDetect ( GraphicGameObject* obj )
{

    if ( CheckCollisionRecs ( _ObjectCollision, obj->GetObject() ) ) {
        return true;
    }
    if ( CheckCollisionRecs ( _NorthCollision, obj->GetSouth() ) ) {
        PLOGW << "North CollisionDetect";
        if ( _IsAttack && _Direction == 1 ) {
// // //             obj->MinusHP();
            _IsAttack = false;
            return true;
        } //else
//             MinusHP();
    }
    if ( CheckCollisionRecs ( _SouthCollision, obj->GetNorth() ) ) {
        PLOGW << "South CollisionDetect";
        if ( _IsAttack && _Direction == 3 ) {
//             obj->MinusHP();
            _IsAttack = false;
            return true;
        } //else
//             MinusHP();
    }
    if ( CheckCollisionRecs ( _EastCollision, obj->GetWest() ) ) {
        PLOGW << "East CollisionDetect";
        if ( _IsAttack && _Direction == 2 ) {
//             obj->MinusHP();
            _IsAttack = false;
            return true;
        } //else
//             MinusHP();
    }
    if ( CheckCollisionRecs ( _WestCollision, obj->GetEast() ) ) {
        PLOGW << "West CollisionDetect";
        if ( _IsAttack && _Direction == 4 ) {
//             obj->MinusHP();
            _IsAttack = false;
            return true;
        } //else
//             MinusHP();
    }

    return false;
}

void RPG::Player::SetPosition(float x, float y) {
    _Position.SetX(x);
    _Position.SetY(y);
}

Rectangle RPG::Player::GetActionRect(){
    return _ObjectCollision;
}

Rectangle RPG::Player::GetCollisionRect(){
    return _ObjectCollision;
}
