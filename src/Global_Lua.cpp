#include "Global_Lua.hpp"
#include "Game.hpp"
#include "Player.hpp"
#include "StoryNode.hpp"
#include "Settings.hpp"
#include "GameStateMaschine.hpp"
#include "MapManager.hpp"

void RPG::LuaSetup (sol::state *state){
    state->new_usertype<Settings>("Settings",
        "new", sol::no_constructor,
        "Instance", &Settings::Instance,
        "GetFullscreen", &Settings::GetFullScreen,
        "GetFullscreenWidth", &Settings::GetFullScreenWidth,
        "GetFullscreenHeight", &Settings::GetFullScreenHeight,
        "GetWindowWidth", &Settings::GetWindowWidth,
        "GetWindowHeight", &Settings::GetWindowHeight,
        "GetFPS", &Settings::GetFPS,
        "GetCollisionBoxes", &Settings::GetCollisionBoxes,
        "GetMusicVolume", &Settings::GetMusicVolume,
        "GetEffectVolume", &Settings::GetEffectVolume,
        "SetFullscreen", &Settings::SetFullscreen,
        "SetFullscreenWidth", &Settings::SetFullScreenWidth,
        "SetFullscrennHeight", &Settings::SetFullscreenHeight,
        "SetWindowWidth", &Settings::SetWindowWidth,
        "SetWindowHeight", &Settings::SetWindowHeight,
        "SetFPS", &Settings::SetFPS,
        "SetCollisionBoxes", &Settings::SetCollisionBoxes,
        "SetMusicVolume", &Settings::SetMusicVolume,
        "SetEffectVolume", &Settings::SetEffectVolume,
        "Save", &Settings::Save
        );

    state->new_usertype<Game>("Game",
        "new", sol::no_constructor,
        "Instance", &Game::Instance,
        "ToggleFullscreen", &Game::_ToggleFullscreen,
        "Quit", &Game::Quit
        );

    state->new_usertype<StoryNode>("StoryNode",
        "new", sol::no_constructor,
        "Instance", &StoryNode::Instance,
        "NewNode", &StoryNode::NewNode,
        "IsFinished", &StoryNode::NodeFinished
        );


    state->new_usertype<GameStateMaschine>("StateMaschine",
        "new", sol::no_constructor,
        "Instance", &GameStateMaschine::Instance,
        "PushState", &GameStateMaschine::PushState,
        "ChangeState", &GameStateMaschine::ChangeState,
        "PopState", &GameStateMaschine::PopState
        );
    state->new_usertype<Player>("Player",
        "new", sol::no_constructor,
        "Instance", &Player::Instance,
        "SetPosition", &Player::SetPosition
        );


    state->new_usertype<MapManager>("MapManager",
        "new", sol::no_constructor,
        "Instance", &MapManager::Instance,
        "ChanceToMap", &MapManager::ChangeCurrentMap
        );
//     state->set_function("testStr", &testStr);
//     state->set_function("testInt", &testInt);
//     state->set_function("testFloat", &testFloat);
}
