#include "Dialog.hpp"
#include <sstream>
#include <config.h>
#include "Settings.hpp"

RPG::Dialog* RPG::Dialog::_Instance = 0;

std::vector<std::string> RPG::split_string_by_newline(const std::string& str)
{
    auto result = std::vector<std::string>{};
    auto ss = std::stringstream{str};

    for (std::string line; std::getline(ss, line, '\n');){
        result.push_back(line);

    }

    return result;
}

void RPG::Dialog::DrawDialog(){
    Rectangle rec;
    rec.x = Settings::Instance()->GetWindowWidth()/4;
    rec.y = (Settings::Instance()->GetWindowHeight()/3) * 2;
    rec.width = rec.x *2;
    rec.height = (rec.y / 3);
    DrawRectangleRounded(rec, 0.5, 4, Fade(LIGHTGRAY, 0.85f));

    int line = 0;
    for (unsigned long i = _LinePos; i < _MSG.size(); i++){
        if (line == 4)
            return;
        DrawTextEx(_Font, _MSG[i].c_str(), Vector2{rec.x + 15 , (rec.y +15) + (25*line) }, 25, 1, BLACK);
        line++;
    }
}

RPG::Dialog * RPG::Dialog::Instance(){
    if (_Instance == nullptr){
        _Instance = new Dialog();
    }
    return _Instance;
}


RPG::Dialog::Dialog() :
    _MSGAvaible(false), _MSG(), _Font(), _LinePos(0){
    _Font = LoadFontEx(std::string(std::string (DATAPATH) + std::string("Font.ttf")).c_str(), 25, NULL, 0);
}

void RPG::Dialog::SetMSG(std::string msg){
    _MSG = split_string_by_newline(msg);
    _LinePos = 0;
    _MSGAvaible = true;
}

void RPG::Dialog::Update(){
    if (_MSG.size() > 4){
        if (_LinePos < _MSG.size() - 4){
            if(IsGamepadButtonPressed(0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN)||IsKeyPressed(KEY_DOWN)) {
                _LinePos++;
            }
        }
        else{
            if (IsGamepadButtonPressed(0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN)||IsKeyPressed(KEY_DOWN)) {
                _MSGAvaible = false;;
            }
        }
    }
    else{
        if (IsGamepadButtonPressed(0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN)||IsKeyPressed(KEY_DOWN)) {
            _MSGAvaible = false;;
        }
    }
}

bool RPG::Dialog::Exist(){
    return _MSGAvaible;
}

