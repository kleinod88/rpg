#include "RPGMapParser.hpp"
#include "TextureManager.hpp"
#include "Game.hpp"
#include "TileLayer.hpp"
#include "Log.hpp"
#include "base64.h"
#include "GameObjectFactory.hpp"
#include "ObjectLayer.hpp"
#include "GraphicGameObject.hpp"
#include <tinyxml2.h>
#include <zlib.h>
#include <config.h>

RPG::RPGMap* RPG::RPGMapParser::ParseRPGMap(std::string levelFile) {
    //tinyXML2 daten initialisieren
    tinyxml2::XMLDocument levelDocument;
    levelDocument.LoadFile(levelFile.c_str());

    //Neues MapObjekt erstellen
    RPGMap* newMap = new RPGMap();

    //Anfang des XML-Dokumentes finden
    tinyxml2::XMLElement* root = levelDocument.RootElement();

    //Größe in Pixel, der einzelnen Tiles
    _TileSize = root->IntAttribute("tilewidth");
    //Breite der Karte in anzahl der Tiles
    _Width = root->IntAttribute("width");
    //Höhe der Karte in Anzahl der Tiles
    _Height = root->IntAttribute("height");
    //Berechnen der Levelbreite in Pixel
    newMap->_Width = _TileSize*_Width;
    //Berechnen der Levelhöhe in Pixel
    newMap->_Height = _TileSize*_Height;

    //Die einzelnen Elemente der XML-Datei durchgehen
    for(tinyxml2::XMLElement *e = root->FirstChildElement(); e != nullptr; e = e->NextSiblingElement()) {
        //tilesets Parsen
        if (std::string(e->Value()) == std::string("tileset")) {
            ParseTilesets(e, newMap->GetTilesets());
        }
        //die einzelnen Ebenen der Karte laden
        else if (std::string(e->Value()) == std::string("layer")) {
            if(e->FirstChildElement()->Value() == std::string("data") ||
                    (e->FirstChildElement()->NextSiblingElement() != 0 && e->FirstChildElement()->NextSiblingElement()->Value() == std::string("data")))
            {
                ParseTileLayer(e, newMap);
            }
        }
        //die Objekte auf der Karte laden
        else if (std::string(e->Value()) == std::string("objectgroup")) {
            if(e->FirstChildElement()->Value() == std::string("object")) {
                ParseObjectLayer(e, newMap);
            }
        }
    }
    return newMap;
}


void RPG::RPGMapParser::ParseTilesets(tinyxml2::XMLElement* tilesetRoot, std::vector<Tileset>* tilesets) {
    //add tileset to texturemanager
    std::string assetsTag = DATAPATH;
    TextureManager::Instance()->Load(assetsTag.append(tilesetRoot->FirstChildElement()->Attribute("source")), tilesetRoot->Attribute("name"));

    Tileset tileset;
    tileset.width = tilesetRoot->FirstChildElement()->IntAttribute("width");
    tileset.height = tilesetRoot->FirstChildElement()->IntAttribute("height");
    tileset.firstGridID = tilesetRoot->IntAttribute("firstgid");
    tileset.tileWidth = tilesetRoot->IntAttribute("tilewidth");
    tileset.tileHeight = tilesetRoot->IntAttribute("tileheight");
    tileset.spacing = tilesetRoot->IntAttribute("spacing", 0);
    tileset.margin = tilesetRoot->IntAttribute("margin", 0);


    tileset.name = tilesetRoot->Attribute("name");

    tileset.numColumns = tileset.width / (tileset.tileWidth + tileset.spacing);

    tilesets->push_back(tileset);

}

void RPG::RPGMapParser::ParseTileLayer(tinyxml2::XMLElement* tileElement, RPGMap *rmap) {
    TileLayer* tileLayer = new TileLayer(_TileSize, _Width, _Height, *rmap->GetTilesets() /*tilesets*/);

    std::vector<std::vector<int>> data;

    std::string decodedIDs;
    tinyxml2::XMLElement *dataNode;

    tileLayer->SetName(tileElement->Attribute("name"));
    for (tinyxml2::XMLElement* e = tileElement->FirstChildElement(); e != nullptr; e = e->NextSiblingElement()) {
        if (std::string(e->Value()) == std::string("data")) {
            dataNode = e;
        }
    }

    for (tinyxml2::XMLNode *e = dataNode->FirstChild(); e != nullptr; e = e->NextSibling()) {
        tinyxml2::XMLText *text = e->ToText();
        std::string t = text->Value();
        decodedIDs = base64_decode(t.c_str());
    }

    uLongf numGrids = _Width * _Height * sizeof(int);
    std::vector<unsigned> gids(numGrids);
    uncompress((Bytef*)&gids[0], &numGrids, (const Bytef*)decodedIDs.c_str(), decodedIDs.size());

    std::vector<int> layerRow(_Width);

    for (int j = 0; j < _Height; j++) {
        data.push_back(layerRow);
    }

    for (int rows = 0; rows < _Height; rows++) {
        for ( int cols = 0; cols < _Width; cols++) {
            data[rows][cols] = gids[rows *_Width + cols];
        }
    }

    tileLayer->SetTileIDs(data);
    if (std::string(tileElement->Attribute("name")) == std::string ("Background")) {
        rmap->_BackgroundLayer = tileLayer;
    }
    if (std::string(tileElement->Attribute("name")) == std::string ("Road")) {
        rmap->_RoadLayer = tileLayer;
    }
    if (std::string(tileElement->Attribute("name")) == std::string ("Water")) {
        rmap->_WaterLayer = tileLayer;
    }
    if (std::string(tileElement->Attribute("name")) == std::string ("Collision")) {
        rmap->_CollisionLayer = tileLayer;
    }
    if (std::string(tileElement->Attribute("name")) == std::string ("Sky")) {
        rmap->_SkyLayer = tileLayer;
    }

    rmap->GetLayers()->push_back(tileLayer);
}


void RPG::RPGMapParser::ParseObjectLayer(tinyxml2::XMLElement* pObjectElement, RPGMap* rmap) {
    ObjectLayer* objectLayer = new ObjectLayer();

    for (tinyxml2::XMLElement* e = pObjectElement->FirstChildElement(); nullptr != e; e = e->NextSiblingElement()){
        if (std::string("object") == e->Value()) {
            std::string type = e->Attribute("class");
            GameObject* gameObject = GameObjectFactory::Instance()->Create(type);
            if (gameObject == nullptr) {
                PLOGE << "Konnte Gameobject nicht erstellen: " << e->Attribute("class");
            }
            else {
                PLOGI << "Erstelle Gameobject: " << e->Attribute("class");
                int x = 0;
                int y = 0;
                int width = 0;
                int height = 0;
                std::string scriptFile;
                x = e->IntAttribute("x");
                y = e->IntAttribute("y");
                width = e->IntAttribute("width");
                height = e->IntAttribute("height");
                std::string name = e->Attribute("name");
                for (tinyxml2::XMLElement* properties = e->FirstChildElement(); nullptr != properties; properties = properties->NextSiblingElement()){
                    if (std::string("properties") == properties->Value()){
                        for (tinyxml2::XMLElement* property = properties->FirstChildElement(); nullptr != property; property = property->NextSiblingElement()){
                            if (std::string("script") == property->Attribute("name")) {
                                scriptFile = property->Attribute("value");
                            }
                        }
                    }

                    PLOGI << "Lade GameObject: " << name << " aus lua-script: " <<  scriptFile;
                    PLOGI << "An Position    : " << x << "x" << y;
                    PLOGI << "Brite          : " << width;
                    PLOGI << "Höhe           : " << height;
                    gameObject->SetPosition(x, y);
                    gameObject->SetWidth(width);
                    gameObject->SetHeight(height);
                    gameObject->Load(scriptFile);

                    objectLayer->GetGameObjectList()->push_back(gameObject);
                }
            }
        }
    }
    rmap->GetLayers()->push_back(objectLayer);
    rmap->_ObjectLayer = objectLayer;
}

