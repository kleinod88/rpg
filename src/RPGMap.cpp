/*
 * RPG
 * Copyright (C) 2021   Frank Kartheuser <frank.kartheuser1988@gmail.com>
 * Copyright (C) 2022   Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#include "RPGMap.hpp"
#include "Player.hpp"
#include "TileLayer.hpp"
#include "Layer.hpp"
#include "Log.hpp"

void RPG::RPGMap::Render() {
    _BackgroundLayer->Render();
    _RoadLayer->Render();
    _WaterLayer->Render();
    _CollisionLayer->Render();
    Player::Instance()->Draw();
    _ObjectLayer->Render();
    _SkyLayer->Render();
}

void RPG::RPGMap::Update() {
    _ObjectLayer->Update();
}

std::vector<RPG::Tileset>* RPG::RPGMap::GetTilesets() {
    return &_Tileset;
}

std::vector<RPG::Layer*>* RPG::RPGMap::GetLayers() {
    return &_Layers;
}



bool RPG::RPGMap::Collison(GameObject *object)
{
    return dynamic_cast<TileLayer*>(_CollisionLayer)->CheckCollision(object);
}
