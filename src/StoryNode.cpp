#include "StoryNode.hpp"

RPG::StoryNode* RPG::StoryNode::_Instance = nullptr;


RPG::StoryNode * RPG::StoryNode::Instance() {
    if (_Instance == nullptr) {
        _Instance = new StoryNode();
    }
    return _Instance;
}

void RPG::StoryNode::NewNode(std::string node, bool value) {
    if (_NodeMap.count(node) != 0) {
        _NodeMap[node] = value;
    }
    _NodeMap[node] = value;
}

bool RPG::StoryNode::NodeFinished(std::string node) {
    if (_NodeMap.count(node) != 0) {
        return _NodeMap[node];
    }
    return false;
}

RPG::StoryNode::StoryNode() {
}

RPG::StoryNode::~StoryNode() {
}
