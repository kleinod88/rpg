/*
 * Erik The Viking
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __GATEWAY
#define __GATEWAY

#include "../GraphicGameObject.hpp"
#include "../GameObjectFactory.hpp"
#include "../Log.hpp"
#define SOL_ALL_SAFETIES_ON 1
#include <sol/sol.hpp>


namespace RPG {

class Gateway : public GraphicGameObject
{
public:
    Gateway();

    void Draw();
    void Update();
    void Clean ();

    void ChangeToMap (std::string map);

    void Load(const std::string scriptFile);

    Rectangle GetActionRect() override{return _ObjectCollision;}
    Rectangle GetCollisionRect() override{return _ObjectCollision;}
private:

    std::string _Changeto;

    sol::state lua;

    sol::function _LUA_Update;
    sol::function _LUA_Draw;
    sol::function _LUA_Enter;
};

class GatewayCreator : public BaseCreator {
public:
    GameObject* CreateObject() const {
        return new Gateway();
    }
};

}; //namespace RPG
#endif // ENEMY_H
