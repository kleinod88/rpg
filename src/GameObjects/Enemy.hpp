/*
 * Erik The Viking
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __ENEMY
#define __ENEMY

#include "../GraphicGameObject.hpp"
#include "../GameObjectFactory.hpp"

namespace RPG {

class Enemy : public GraphicGameObject
{
public:
    Enemy();

    void Draw();
    void Update();
    void Clean ();

    void Load(const std::string scriptFile);

    Rectangle GetActionRect() override{return _ObjectCollision;}
    Rectangle GetCollisionRect() override{return _ObjectCollision;}
private:

    int _Frame;

};

class EnemyCreator : public BaseCreator {
public:
    GameObject* CreateObject() const {
        return new Enemy();
    }
};

}; //namespace RPG
#endif // ENEMY_H
