/*
 * Erik The Viking
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Enemy.hpp"
#include "../Player.hpp"
#include <raylib.h>

RPG::Enemy::Enemy()
    : GraphicGameObject()
{
    _Velocity.SetY(0.0f);
    _Velocity.SetX(0.6f);
    _Frame = 0;
}

void RPG::Enemy::Draw()
{
    if (_Frame < 7)
        _CurrentFrame = 0;
    else if (_Frame < 14 )
        _CurrentFrame = 1;
    else if (_Frame < 21)
        _CurrentFrame = 2;
    else if (_Frame < 28)
        _CurrentFrame = 3;
    else {
        _Frame = 0;
    }
    GraphicGameObject::Draw();
}

void RPG::Enemy::Clean()
{
    GraphicGameObject::Clean();
}

void RPG::Enemy::Update()
{
    if (_Position.GetX() < 350)
    {
        _Velocity.SetX(0.6f);
        _CurrentRow = 3;
        _Direction = 2;
    }
    if (_Position.GetX() > 500)
    {
        _Velocity.SetX(-0.6f);
        _CurrentRow = 2;
        _Direction = 4;
    }

    if (_Frame < 60)
        _Frame++;
    else
        _Frame = 0;

    _Position += _Velocity;
    _ObjectCollision.x = _Position.GetX() + ((_Width - 10)/2);
    _ObjectCollision.y = _Position.GetY() + (_Height-10);
    _NorthCollision.x = _Position.GetX()-2;
    _NorthCollision.y = _Position.GetY()-1;
    _EastCollision.x = _Position.GetX() + _Width - 2;
    _EastCollision.y = _Position.GetY() - 1;
    _SouthCollision.x = _Position.GetX() - 2;
    _SouthCollision.y = _Position.GetY() + _Height - 2;
    _WestCollision.x = _Position.GetX() - 2;
    _WestCollision.y = _Position.GetY() - 1;

    //FIXME das muss weg hier und zur Kollision
    Player::Instance()->CollisionDetect(this);
}

void RPG::Enemy::Load(const std::string scriptFile)
{
    GraphicGameObject::Load(scriptFile);

}
