/*
 * Erik The Viking
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Gateway.hpp"
#include "../Dialog.hpp"
#include "../Player.hpp"
#include "../Game.hpp"
#include "../GameStates/PlayState.hpp"
#include "../MapManager.hpp"
#include "../StoryNode.hpp"
#include "../Global_Lua.hpp"
#include <config.h>
#include <raylib.h>

RPG::Gateway::Gateway()
    : GraphicGameObject(){
}

void RPG::Gateway::Draw()
{
    GraphicGameObject::Draw();
}

void RPG::Gateway::Clean()
{
    GraphicGameObject::Clean();
}

void RPG::Gateway::Update()
{
    if (Player::Instance()->CollisionDetect(this)) {
        _LUA_Enter();
    }
    _LUA_Update();
}

void RPG::Gateway::ChangeToMap(std::string map){
    PLOGW << "Change to map: " << map;
 //   MapManager::Instance()->ChangeCurrentMap(map);
}




void RPG::Gateway::Load(const std::string scriptFile){
    lua.open_libraries(sol::lib::base);
    lua.open_libraries(sol::lib::string);

    LuaSetup(&lua);


    lua.set_function("ChangeToMap", &Gateway::ChangeToMap);
    lua.script_file (std::string (DATAPATH) + scriptFile);

    _ObjectCollision.x = _Position.GetX() + ((_Width - 10)/2);
    _ObjectCollision.y = _Position.GetY() + (_Height-10);
    _ObjectCollision.width = 10;
    _ObjectCollision.height = 10;

    _LUA_Update = lua["Update"];
    _LUA_Draw = lua["Draw"];
    _LUA_Enter = lua["EnterGateway"];

}
