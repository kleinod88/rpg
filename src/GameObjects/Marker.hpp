/*
 * Erik The Viking
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __MARKER
#define __MARKER

#include "../GraphicGameObject.hpp"
#include "../GameObjectFactory.hpp"
#define SOL_ALL_SAFETIES_ON 1
#include <sol/sol.hpp>
#include "Log.hpp"

namespace RPG {

class Marker : public GraphicGameObject
{
public:
    Marker();

    void Draw();
    void Update();
    void Clean ();

    void SetText (std::string text);

    void Load(const std::string script);

    void SetPosition (float x, float y)
    {
        GraphicGameObject::SetPosition(x, y);
    }
    void SetWidth (float w)
    {
        GraphicGameObject::SetWidth(w);
    }
    void SetHeight (float h){
        GraphicGameObject::SetHeight(h);
    }
    Rectangle GetActionRect() override{return _ObjectCollision;}
    Rectangle GetCollisionRect() override{return _ObjectCollision;}


private:

    std::string _Text;

    sol::state lua;

    sol::function _LUA_Update;
    sol::function _LUA_Draw;
    sol::function _LUA_GetText;

};

class MarkerCreator : public BaseCreator {
public:
    GameObject* CreateObject() const {
        return new Marker();
    }
};

}; //namespace RPG
#endif // ENEMY_H
