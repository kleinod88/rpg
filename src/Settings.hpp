/*
 * RPG
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 * Copyright (C) 2022  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __SETTINGS
#define __SETTINGS



namespace RPG {

class Settings {
public:
    static Settings* Instance();

    Settings();

    bool Load ();
    void Save ();

    void SetFullscreen(bool fullscreen);
    void SetFullScreenWidth(int width);
    void SetFullscreenHeight(int height);
    void SetWindowWidth(int width);
    void SetWindowHeight(int height);
    void SetFPS(int fps);
    void SetCollisionBoxes(bool boxes);
    void SetMusicVolume(float volume);
    void SetEffectVolume(float volume);

    bool GetFullScreen();
    int GetFullScreenWidth();
    int GetFullScreenHeight();
    int GetWindowWidth();
    int GetWindowHeight();
    int GetFPS();
    bool GetCollisionBoxes();
    float GetMusicVolume();
    float GetEffectVolume();

private:

    static Settings* _Instance;
    
    bool _Fullscreen;
    bool _CollisionBoxes;
    int _FullscreenWidth;
    int _FullscreenHeight;
    int _WindowedWidth;
    int _WindowedHeight;
    int _FPS;

    float _MusicVolume;
    float _EffectVolume;
}; //class Settings
}; //namespace RPG
#endif // __OPTIONS
