#include "Game.hpp"
#include "Log.hpp"
#include "Settings.hpp"

int main ([[maybe_unused]] int argc,[[maybe_unused]]  char* argv[]) {

    if (RPG::Game::Instance()->Init("RPG Demo")) {
        while (RPG::Game::Instance()->Running()) {
            RPG::Game::Instance()->HandleEvents();
            RPG::Game::Instance()->Update();
            RPG::Game::Instance()->Render();
        }
    }
    else {
        return 1;
    }
    return 0;
}
