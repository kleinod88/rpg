/*
 * RPG
 * Copyright (C) 2021   Frank Kartheuser <frank.kartheuser1988@gmail.com>
 * Copyright (C) 2022   Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MapManager.hpp"
#include "RPGMapParser.hpp"
#include <raylib.h>
#include <string>
#include "Log.hpp"

RPG::MapManager* RPG::MapManager::_Instance = 0;

bool RPG::MapManager::LoadMap(std::string fileName, std::string id) {
    //Prüfen ob es schon eine Karte mit der id gibt
    if (Exist(id)){
        PLOGW << "Karte mit ID "  << id << " existiert bereits";
        return false;
    }

    //Karte aus Datei laden
    RPGMapParser parser;
    RPGMap *newMap = parser.ParseRPGMap(fileName);

    //Prüfen ob Karte geladen wurde
    if (newMap == nullptr){
        PLOGE << "Karte " << id << " konnte nicht geladen werden";
        return false;
    }

    //Karte zum Stapel hinzufügen
    _Maps[id] = newMap;
    PLOGI << "Karte erfolgreich geladen: " << id;
    return true;
}

void RPG::MapManager::Draw () {
    _CurrentMap->Render();
}

bool RPG::MapManager::ChangeCurrentMap(std::string id){
    if (Exist(id)){
        _CurrentMap = _Maps[id];
        return true;
    }
    PLOGW << "Konnte nicht zur Karte: " << id << " wchseln: nicht vorhanden";
    return false;
}

bool RPG::MapManager::RemoveMap(std::string id){
    return false;
}

void RPG::MapManager::Update(){
    _CurrentMap->Update();
}

RPG::MapManager * RPG::MapManager::Instance(){
    if (_Instance == nullptr){
        _Instance = new MapManager();
    }
    return _Instance;
}

bool RPG::MapManager::Collision(GameObject *object){
    return _CurrentMap->Collison(object);
}

int RPG::MapManager::GetWidth(){
    return _CurrentMap->GetWidth();
}

int RPG::MapManager::GetHeight(){
    return _CurrentMap->GetHeight();
}


RPG::MapManager::MapManager() {

}

RPG::MapManager::~MapManager()
{
}

bool RPG::MapManager::Exist(std::string id) {
    if (_Maps.count(id) == 0)
        return false;
    else
        return true;
}

