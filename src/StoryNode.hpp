#ifndef __STORYNODE
#define __STORYNODE

#include <string>
#include <map>

namespace RPG {

class StoryNode
{
public:
    static StoryNode* Instance();

    bool NodeFinished (std::string node);

    void NewNode (std::string node, bool value);

private:
    std::map<std::string,bool> _NodeMap;


    static StoryNode *_Instance;

    StoryNode();
    ~StoryNode();
};
}; //namespace RPG
#endif // __STORYNODE
