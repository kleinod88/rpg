#ifndef DIALOG_H
#define DIALOG_H

#include <raylib.h>
#include <string>
#include <vector>

namespace RPG{
/**
 * @todo write docs
 */

std::vector<std::string> split_string_by_newline(const std::string& str);

class Dialog
{
public:
    static Dialog* Instance();

    void SetMSG (std::string msg);

    void Update();

    bool Exist();

    void DrawDialog ();

private:

    static Dialog *_Instance;
    bool _MSGAvaible;
    std::vector<std::string> _MSG;
    Font _Font;
    int _LinePos;
    Dialog();
};
};//namespace RPG
#endif // DIALOG_H
