/*
 * RPG
 * Copyright (C) 2021 - 2022   Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __VECTOR2D
#define __VECTOR2D

#include <math.h>

class Vector2D {
public:
    Vector2D(float x, float y) : _X(x), _Y(y) {}

    float GetX() {
        return _X;
    }
    float GetY() {
        return _Y;
    }

    void SetX(float x) {
        _X=x;
    }
    void SetY(float y) {
        _Y=y;
    }

    float Length() {
        return sqrt(_X*_X + _Y*_Y);
    }

    Vector2D operator+ (const Vector2D& rhs) const {
        return Vector2D(_X +rhs._X, _Y + rhs._Y);
    }

    friend Vector2D& operator+=(Vector2D& lhs, const Vector2D& rhs) {
        lhs._X += rhs._X;
        lhs._Y += rhs._Y;
        return lhs;
    }

    Vector2D operator* (float scaler) {
        return Vector2D(_X*scaler, _Y*scaler);
    }

    Vector2D& operator*= (float scaler) {
        _X *= scaler;
        _Y *= scaler;
        return *this;
    }

    Vector2D operator- (const Vector2D& rhs) const {
        return Vector2D(_X - rhs._X, _Y - rhs._Y);
    }

    friend Vector2D& operator-= (Vector2D& lhs, const Vector2D& rhs) {
        lhs._X -= rhs._X;
        lhs._Y -= rhs._Y;
        return lhs;
    }


    Vector2D operator/ (float scaler) {
        return Vector2D(_X / scaler, _Y / scaler);
    }

    Vector2D operator/= (float scaler) {
        _X /= scaler;
        _Y /= scaler;
        return *this;
    }

    void Normalize() {
        float l = Length();
        if ( l > 0) //never attempt to divvde by 0
        {
            (*this) *= 1 / l;
        }
    }

private:

    float _X;
    float _Y;
};

#endif //__VECTOR_2D
