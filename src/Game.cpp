#include "Game.hpp"
#include "GameStateMaschine.hpp"
#include "Settings.hpp"
#include "FrameworkSettings.hpp"
#include "GameObjects/Enemy.hpp"
#include "GameObjects/Gateway.hpp"
#include "GameObjectFactory.hpp"
#include <config.h>
#include "GameObjects/Marker.hpp"
#include <raylib.h>
#include "Log.hpp"

RPG::Game* RPG::Game::_Instance = nullptr;

RPG::Game::Game() :
    _Running(false), _ChangeFullscreen(false), _IsFullscreen(false){
}



bool RPG::Game::Init(const char* title) {
    //Als erstes Loggingsystem Initialisieren
    InitLog();

    //Optionen aus datei laden
    Settings::Instance()->Load();
    FrameworkSettings::Instance()->Load();


    //FIXME eventuell noch zu optionen hinzufügen
    SetTraceLogLevel(8);

    //Fenster erstellen
    if (Settings::Instance()->GetFullScreen()){
        SetConfigFlags(FLAG_FULLSCREEN_MODE);
        InitWindow(Settings::Instance()->GetFullScreenWidth(), Settings::Instance()->GetFullScreenHeight(), title);
    }
    else{
        InitWindow(Settings::Instance()->GetWindowWidth(), Settings::Instance()->GetWindowHeight(), title);
    }

    //Testen ob das Fenster erstellt werden konnte
    if (IsWindowReady()) {
        PLOGI << "Erfolgreich Initialisiert";
    }
    else {
        PLOGE << "Fehler beim Initialisieren";
        return false;
    }

    //FIXME Die Creators sollten aus einer datei geladen werden
    //Laden der ObjectCreators
    GameObjectFactory::Instance()->RegisterType("Enemy", new EnemyCreator());
    GameObjectFactory::Instance()->RegisterType("Gateway", new GatewayCreator());
    GameObjectFactory::Instance()->RegisterType("Marker", new MarkerCreator());


    //Bilder pro Sekunde setzen
    SetTargetFPS(Settings::Instance()->GetFPS());
    
    //Audiogerät initialisieren
    InitAudioDevice();

    //GamesteteMaschine Erstellen und ersten State laden
    if (FrameworkSettings::Instance()->GetShowSplash()){
        GameStateMaschine::Instance()->ChangeState("Splash");
    }
//     else if (FrameworkSettings::Instance()->GetStartToPlay()){
//         GameStateMaschine::Instance()->ChangeState("Play", "Splash.lua");}
    else
        GameStateMaschine::Instance()->ChangeState("Menu", "Main.menu");

    //Mauszeiger verstecken
    //HiddeMou();
    SetMousePosition(0, 0);

    SetConfigFlags(FLAG_MSAA_4X_HINT);
    SetExitKey(KEY_F4);
    //Dialoge Initialisieren
//     Dialog::Instance()->Init();


    //bis jetzt war alles gut, also läuft das spiel
    _Running = true;

    return _Running;
}

void RPG::Game::Render() {
    BeginDrawing();
    ClearBackground(BLUE);
    GameStateMaschine::Instance()->Render();
    EndDrawing();
}

void RPG::Game::HandleEvents() {
    if (IsKeyPressed(KEY_F))
    {
        _ToggleFullscreen();
    }
}

void RPG::Game::Clean() {
//     DLOG << "cleaning game\n";
}

void RPG::Game::Quit() {
    _Running = false;
}


void RPG::Game::Update() {

    if (WindowShouldClose()){
        _Running = false;
    }
    GameStateMaschine::Instance()->Update();
}

RPG::Game* RPG::Game::Instance() {
    if (_Instance == nullptr) {
        _Instance = new Game();
    }
    return _Instance;
}


bool RPG::Game::Running() {
    return _Running;
}

void RPG::Game::_ToggleFullscreen() {
    ToggleFullscreen();
    if (IsWindowFullscreen()){
        SetWindowSize(Settings::Instance()->GetFullScreenWidth(), Settings::Instance()->GetFullScreenHeight());
    }
    else{
        SetWindowSize(Settings::Instance()->GetWindowWidth(), Settings::Instance()->GetWindowHeight());
    }
}
