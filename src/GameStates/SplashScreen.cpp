/*
 * RPG
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "SplashScreen.hpp"
#include "../GameStateMaschine.hpp"
#include <raylib.h>
#include <raygui.h>
#include <config.h>
#include "../Log.hpp"

const std::string RPG::Splashscreen::_ID = "SPLASHSCREEN";



bool RPG::Splashscreen::OnEnter([[maybe_unused]] std::string file)
{
    _NameLable.x = (float)GetScreenWidth() / 4;
    _NameLable.y = (float)GetScreenHeight() / 5;
    _NameLable.width = _ExtraLable.width = 400; //keine ahnung ob das so geht
    _NameLable.height = _ExtraLable.height = 150;
    _ExtraLable.x = GetScreenWidth();
    _ExtraLable.y = _NameLable.y + _NameLable.height;

    _ExtraSpeed = (GetScreenWidth() - _NameLable.x) / 140;

    //FIXME Fontdatei muss universeller werden
    Font font = LoadFontEx(std::string(std::string (DATAPATH) + std::string("Font.ttf")).c_str(), 250, NULL, 0);

    GuiSetFont(font);
    GuiSetStyle(DEFAULT, TEXT_SIZE, 200);
    return true;
}


bool RPG::Splashscreen::OnExit()
{
    return true;
}

std::string RPG::Splashscreen::GetStateID() const
{
    return _ID;
}

void RPG::Splashscreen::Render()
{
    GuiSetStyle(LABEL, TEXT_COLOR_NORMAL, ColorToInt(SKYBLUE) );
    GuiLabel(_NameLable, "RPG");
    GuiSetStyle(LABEL, TEXT_COLOR_NORMAL, ColorToInt(RED) );
    GuiLabel(_ExtraLable, "DEMO");
}

void RPG::Splashscreen::Update()
{
    _Frame++;
    if (_Frame < 140)
        _ExtraLable.x = _ExtraLable.x - _ExtraSpeed;
    else
        GameStateMaschine::Instance()->ChangeState("Menu", "Main.menu");
}





