#include "PlayState.hpp"
#include "../Game.hpp"
#include "../TextureManager.hpp"
#include "../Player.hpp"
#include "../Log.hpp"
#include "../RPGMapParser.hpp"
#include "../Settings.hpp"
#include "../MapManager.hpp"
#include "../Dialog.hpp"
// #include "../GameObjects/Enemy.hpp"
#include "../GameStateMaschine.hpp"
#include <raylib.h>
#include <config.h>

const std::string RPG::PlayState::_PlayID = "PLAY";

bool warenInPause;

void RPG::PlayState::Update() {
    if (Dialog::Instance()->Exist()){
        Dialog::Instance()->Update();
        return;
    }
    UpdateMusicStream(_BackgroundMusic);
    if (IsGamepadButtonPressed(0, GAMEPAD_BUTTON_MIDDLE_RIGHT)||IsKeyDown(KEY_ESCAPE)) {
        warenInPause = true;
        GameStateMaschine::Instance()->PushState("Menu", "Pause.menu");
    }

    if (warenInPause){
        SetMusicVolume (_BackgroundMusic, Settings::Instance()->GetMusicVolume());
        warenInPause = false;
    }

    Vector2D oldPlayerPosition =  Player::Instance()->GetPosition();

    Player::Instance()->Update();

    if (MapManager::Instance()->Collision(Player::Instance())) {
        Player::Instance()->SetPosition(oldPlayerPosition.GetX(), oldPlayerPosition.GetY());
    }

    MapManager::Instance()->Update();
    //position geht von den orginal pixeln aus, nicht von denen des bildschirms
    float x = Player::Instance()->GetPosition().GetX() - 224;
    float y = Player::Instance()->GetPosition().GetY() - 128;

    if (x < 0.0f )
        x = 0.0f;
    if (y < 0.0f)
        y = 0.0f;

    //FIXME Mapgröße automatisch ermitteln
    if (x >= MapManager::Instance()->GetWidth() - GetScreenWidth()/2)
        x = MapManager::Instance()->GetWidth() - GetScreenWidth()/2;
    if (y >= MapManager::Instance()->GetHeight() - GetScreenHeight()/2)
        y = MapManager::Instance()->GetHeight() - GetScreenHeight()/2;

    _Camera.target = { x, y };
    if (IsWindowFullscreen()) {
        _Camera.zoom = 4;
    }
    else {
        _Camera.zoom = 2;
    }

    if (!IsMusicStreamPlaying(_BackgroundMusic)) {
        PlayMusicStream(_BackgroundMusic);
    }
}

void RPG::PlayState::Render() {
    BeginMode2D(_Camera);
    MapManager::Instance()->Draw();
    //     for (int i = 0; i < Player::Instance()->GetHP(); i++) {
//         TextureManager::Instance()->DrawFrame("heart",_Camera.target.x+4+(i*16),_Camera.target.y+4,18, 16,0,0);
//     }
    EndMode2D();
    if (Dialog::Instance()->Exist()){
        Dialog::Instance()->DrawDialog();
    }
}

RPG::PlayState::PlayState(){
}


bool RPG::PlayState::OnEnter([[maybe_unused]] std::string file) {
    //FIXME Texturen aus Datei laden und nicht mehr Händisch
    if(!TextureManager::Instance()->Load(std::string(DATAPATH) + std::string("BlueKnight.png"), "BlueKnight")) {
        PLOGW << "Can't load player sprite";
    }
    if(!TextureManager::Instance()->Load(std::string(DATAPATH) + std::string("NPC.png"), "NPC")) {
        PLOGW << "Can't load player sprite";
    }
    if(!TextureManager::Instance()->Load(std::string(DATAPATH) + std::string("Hero.png"), "player")) {
        PLOGW << "Can't load player sprite";
    }
    _BackgroundMusic =  LoadMusicStream(std::string(std::string(DATAPATH) + std::string("BackgroundmusicDemo.ogg")).c_str());
    PlayMusicStream(_BackgroundMusic);
    SetMusicVolume (_BackgroundMusic, Settings::Instance()->GetMusicVolume());

    //FIXME: MapManager um zwischen verschiedenen Karten zu wechseln
    MapManager::Instance()->LoadMap(std::string(DATAPATH) + std::string("Demo.tmx"), "World");
    MapManager::Instance()->LoadMap(std::string(DATAPATH) + std::string("House.tmx"), "House1");
    MapManager::Instance()->ChangeCurrentMap("World");

    GameObject *player = Player::Instance();
    player->Load("n/a");

    Vector2 d = {Player::Instance()->GetPosition().GetX() + GetScreenWidth()/4, Player::Instance()->GetPosition().GetY() + GetScreenHeight()/4 };
    Vector2 f = { 0.0f, 0.0f};
    _Camera.target = d;
    _Camera.offset = f;
    _Camera.rotation = 0.0f;
    _Camera.zoom = 4.0f;

    Dialog::Instance()->SetMSG("Hallo und Willkommen zu dieser Demo.\nSie dient nur zum Testen.\nWirklichen Spieleinhalt, wie z.B eine\nSuper tolle geschichte wirst du hier\nvergebens suchen\ndennoch viel Spaß beim spielen");
    return true;
}

bool RPG::PlayState::OnExit() {
    for (unsigned long i = 0; i < _Objects.size(); i++) {
        _Objects[i]->Clean();
    }
    return true;
}

std::string RPG::PlayState::GetStateID() const {
    return _PlayID;
}

