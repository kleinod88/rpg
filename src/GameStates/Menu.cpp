#include "Menu.hpp"
// #include "PlayState.hpp"
#include "../Game.hpp"
#include "../Log.hpp"
#include "../Settings.hpp"
#include "../Global_Lua.hpp"
#include "FrameworkSettings.hpp"
#include <raylib.h>
#define RAYGUI_IMPLEMENTATION
#include <raygui.h>
#include <sstream>
#include <config.h>



RPG::Button::Button(std::string text, float x, float y, float width, float height){
    _Activ = false;
    _Text = text;
    _Rect.x = x;
    _Rect.y = y;
    _Rect.width = width;
    _Rect.height = height;
}

void RPG::Button::Draw(){
    if (_Activ){
        GuiSetState(STATE_FOCUSED);
    }
    else{
        GuiSetState(STATE_NORMAL);
    }

    GuiButton(_Rect, _Text.c_str());
    GuiSetState(STATE_NORMAL);
}

void RPG::Button::SetActiv(bool activ){
    _Activ = activ;
}

RPG::CheckBox::CheckBox(std::string text, float x, float y, float width, float height, bool checked){
    _Activ = false;
    _Rect.x = x;
    _Rect.y = y;
    _Rect.width = width;
    _Rect.height = height;
    _Check = checked;
    _Text = text;
}

bool RPG::CheckBox::IsActivated(){
    return _Check;
}

void RPG::CheckBox::Toggle(bool n){
    _Check = n;
}

void RPG::CheckBox::Draw(){
    if (_Activ){
        GuiSetState(STATE_FOCUSED);
    }
    else{
        GuiSetState(STATE_NORMAL);
    }

    GuiCheckBox(_Rect, _Text.c_str(),_Check);
    GuiSetState(STATE_NORMAL);

}

void RPG::CheckBox::SetActiv(bool activ){
    _Activ = activ;
}

RPG::Slider::Slider(std::string text, float x, float y, float width, float height, float currentVal, float minVal, float maxVal){
    _Activ = false;
    _CurrentValue = currentVal;
    _MinValue = minVal;
    _MaxValue = maxVal;
    _Rect.x = x;
    _Rect.y = y;
    _Rect.width = width;
    _Rect.height = height;
    _Text = text;
}

float RPG::Slider::GetValue(){
    return _CurrentValue;
}

void RPG::Slider::SetValue(float val){
    if (val <= 0)
        val = 0.0;
    if (val > 1)
        val = 1.0;
    if (val == 2.77556e-17)
        val = 0;
    _CurrentValue = val;
}


void RPG::Slider::Draw(){
    if (_Activ){
        GuiSetState(STATE_FOCUSED);
    }
    else{
        GuiSetState(STATE_NORMAL);
    }

    std::stringstream ss;
    int i = 100*_CurrentValue;
    if (_CurrentValue == 0.0)
        ss << "0";
    else
        ss << i;

    GuiSliderBar(_Rect, _Text.c_str(),ss.str().c_str(),_CurrentValue, _MinValue, _MaxValue);
    GuiSetState(STATE_NORMAL);
}


void RPG::Slider::SetActiv(bool activ){
    _Activ = activ;
}


void RPG::Menu::Update()
{
    if (IsKeyPressed(KEY_DOWN)||IsGamepadButtonPressed(0, GAMEPAD_BUTTON_LEFT_FACE_DOWN)) {
        _LUA_Key_Down();
    }
    if (IsKeyPressed(KEY_UP)||IsGamepadButtonPressed(0, GAMEPAD_BUTTON_LEFT_FACE_UP)) {
        _LUA_Key_Up();
    }
    if (IsKeyPressed(KEY_LEFT)||IsGamepadButtonPressed(0, GAMEPAD_BUTTON_LEFT_FACE_LEFT)) {
        _LUA_Key_Left();
    }
    if (IsKeyPressed(KEY_RIGHT)||IsGamepadButtonPressed(0, GAMEPAD_BUTTON_LEFT_FACE_RIGHT)) {
        _LUA_Key_Right();
    }
    if (IsKeyPressed(KEY_ENTER)||IsGamepadButtonPressed(0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN)) {
        _LUA_Key_Activate();
    }
    if (IsKeyPressed(KEY_ESCAPE)||IsGamepadButtonPressed(0, GAMEPAD_BUTTON_RIGHT_FACE_UP)) {
        _LUA_Key_Back();
    }
    _LUA_Update();
}

void RPG::Menu::Render()
{
    _LUA_Draw();

    if (FrameworkSettings::Instance()->GetShowVersion())
        DrawText(std::string(std::string("Version: ") + std::string(RPG_VERSION)).c_str(), 20, Settings::Instance()->GetWindowHeight()-20, 20, RED);
}

std::string RPG::Menu::GetStateID() const
{
    return _ID;
}




void RPG::Menu::SetID(std::string id){
    PLOGE << "Setzte ID zu " << id;
    _ID = id;
}



bool RPG::Menu::OnEnter(std::string file)
{
    _Font = LoadFontEx(std::string(std::string (DATAPATH) + std::string("Font.ttf")).c_str(), 25, NULL, 0);
    GuiSetFont(_Font);

    lua.open_libraries(sol::lib::base);
    lua.open_libraries(sol::lib::string);

    lua.new_usertype<Button>("Button",
                            sol::constructors<Button(std::string, float, float, float, float)>(),
                             "Draw", &Button::Draw,
                             "SetActiv", &Button::SetActiv);
    lua.new_usertype<Slider>("Slider",
                            sol::constructors<Slider(std::string, float, float, float, float, float, float, float)>(),
                             "Draw", &Slider::Draw,
                             "SetActiv", &Slider::SetActiv,
                             "GetValue", &Slider::GetValue,
                             "SetValue", &Slider::SetValue);
    lua.new_usertype<CheckBox>("CheckBox",
                            sol::constructors<CheckBox(std::string, float, float, float, float, bool)>(),
                             "Draw", &CheckBox::Draw,
                             "SetActiv", &CheckBox::SetActiv,
                             "IsActivated", &CheckBox::IsActivated,
                             "Toggle", &CheckBox::Toggle);

    
    LuaSetup(&lua);
    lua.set_function("SetID", &Menu::SetID);
    lua.script_file (std::string (DATAPATH) + std::string ("Menu/") + file);


    _ID = lua["MENUID"];

    _LUA_Update = lua["Update"];
    _LUA_Draw = lua["Draw"];
    _LUA_Key_Up = lua["Key_Up"];
    _LUA_Key_Down = lua["Key_Down"];
    _LUA_Key_Left = lua["Key_Left"];
    _LUA_Key_Right = lua["Key_Right"];
    _LUA_Key_Activate = lua["Activate"];
    _LUA_Key_Back = lua["Back"];


    return true;
}

bool RPG::Menu::OnExit(){
}

