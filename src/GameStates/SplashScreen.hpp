/*
 * RPG
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __SPLASHSCREEN
#define __SPLASHSCREEN

#include "../GameState.hpp"
#include <vector>
#include <raylib.h>

namespace RPG {

class Splashscreen : public GameState
{
public:
    virtual void Update();
    virtual void Render();
    virtual bool OnEnter([[maybe_unused]] std::string file = "");
    bool OnExit();

    virtual std::string GetStateID() const;

private:

    Font _Font;
    Rectangle _NameLable, _ExtraLable;

    int _Frame;
    int _ExtraSpeed;

    static const std::string _ID;


};

};
#endif // __SPLASHSCREEN

