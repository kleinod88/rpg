/*
 * RPG
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __PLAYSTATE
#define __PLAYSTATE

#include "../GameState.hpp"
#include "../GraphicGameObject.hpp"
#include "../RPGMap.hpp"
#include "../Player.hpp"
#include <raylib.h>
#include <vector>

namespace RPG {

class PlayState : public GameState
{
public:
    PlayState ();
    virtual void Update();
    virtual void Render();

    virtual bool OnEnter([[maybe_unused]] std::string file = "");
    virtual bool OnExit();

    virtual std::string GetStateID() const;
    bool CheckCollision (GraphicGameObject *obj1, GraphicGameObject *obj2);

private:

    static const std::string _PlayID;
    Camera2D _Camera;
    std::vector<GameObject*> _Objects;

    Music _BackgroundMusic;


    int _CurrentMap;

};

}; //namespace RPG
#endif // PLAYSTATE
