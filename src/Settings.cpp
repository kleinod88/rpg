/*
 * RPG
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 * Copyright (C) 2022  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <fstream>
#include <sol/sol.hpp>
#include "Settings.hpp"
#define SOL_ALL_SAFETIES_ON 1
#include <config.h>
#include "Log.hpp"


RPG::Settings* RPG::Settings::_Instance = nullptr;


RPG::Settings* RPG::Settings::Instance() {
    if (_Instance == nullptr)
        _Instance = new Settings();
    return _Instance;
}

RPG::Settings::Settings()
    : _Fullscreen(false), _CollisionBoxes(true), _FullscreenWidth(1920), _FullscreenHeight(1080),
      _WindowedWidth(800), _WindowedHeight(600), _FPS(60), _MusicVolume(0.5f), _EffectVolume(0.5f)
{
}

bool RPG::Settings::Load() {
    sol::state lua;
    lua.open_libraries(sol::lib::base);
    lua.open_libraries(sol::lib::string);

    lua.script_file (std::string (DATAPATH) + std::string ("Settings/") + std::string("Settings.lua"));
    _Fullscreen = lua.get<bool>("FULLSCREEN");
    _CollisionBoxes = lua.get<bool>("COLLISIONBOXES");
    _FullscreenWidth =  lua.get<int>("FULLSCREENWIDTH");
    _FullscreenHeight =  lua.get<int>("FULLSCREENHEIGHT");
    _WindowedWidth =  lua.get<int>("WINDOWWIDTH");
    _WindowedHeight =  lua.get<int>("WINDOWHEIGHT");
    _MusicVolume =  lua.get<float>("MUSICVOLUME");
    _EffectVolume =  lua.get<float>("EFECTVOLUME");

    return true;
}

void RPG::Settings::Save(){
    std::ofstream file;
    file.open (std::string (DATAPATH) + std::string ("Settings/") + std::string("Settings.lua"));
    file << "FULLSCREEN = ";
    if (_Fullscreen)
        file << "true\n";
    else
        file << "false\n";
    file << "COLLISIONBOXES = ";
    if (_CollisionBoxes)
        file << "true\n";
    else
        file << "false\n";
    file << "FULLSCREENWIDTH = " << _FullscreenWidth << "\n";
    file << "FULLSCREENHEIGHT = " << _FullscreenHeight << "\n";
    file << "WINDOWWIDTH = " << _WindowedWidth << "\n";
    file << "WINDOWHEIGHT = " << _WindowedHeight << "\n";
    file << "MUSICVOLUME = " << _MusicVolume << "\n";
    file << "EFECTVOLUME = " << _EffectVolume << "\n";
    file.close();
}


void RPG::Settings::SetFullscreen(bool fullscreen) {
    _Fullscreen = fullscreen;
}

void RPG::Settings::SetCollisionBoxes(bool boxes) {
    _CollisionBoxes = boxes;
}

void RPG::Settings::SetFullScreenWidth(int width) {
    _FullscreenWidth = width;
}

void RPG::Settings::SetFullscreenHeight(int height) {
    _FullscreenHeight = height;
}

void RPG::Settings::SetWindowWidth(int width) {
    _WindowedWidth = width;
}

void RPG::Settings::SetWindowHeight(int height) {
    _WindowedHeight = height;
}

void RPG::Settings::SetFPS(int fps){
    _FPS = fps;
}

void RPG::Settings::SetMusicVolume(float volume) {
    _MusicVolume = volume;
}

void RPG::Settings::SetEffectVolume(float volume) {
    _EffectVolume = volume;
}

bool RPG::Settings::GetFullScreen() {
    return _Fullscreen;
}

bool RPG::Settings::GetCollisionBoxes() {
    return _CollisionBoxes;
}

int RPG::Settings::GetFullScreenWidth() {
    return _FullscreenWidth;
}

int RPG::Settings::GetFullScreenHeight() {
    return _FullscreenHeight;
}

int RPG::Settings::GetWindowWidth() {
    return _WindowedWidth;
}

int RPG::Settings::GetWindowHeight() {
    return _WindowedHeight;
}

int RPG::Settings::GetFPS(){
    return _FPS;
}
float RPG::Settings::GetMusicVolume() {
    return _MusicVolume;
}

float RPG::Settings::GetEffectVolume() {
    return _EffectVolume;
}

